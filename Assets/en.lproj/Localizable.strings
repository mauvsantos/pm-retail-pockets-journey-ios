// File for localized content

"pockets.tab.title" = "Pockets";

// MARK: - Common Errors
"pockets.edgeCase.errors.notConnected.title" = "No internet connection";
"pockets.edgeCase.errors.notConnected.subtitle" = "It seems you’re not connected to a network";
"pockets.edgeCase.errors.notConnected.actionButton.title" = "Try again";

"pockets.edgeCase.errors.loadingFailure.title" = "Oops, loading failed";
"pockets.edgeCase.errors.loadingFailure.subtitle" = "Something went wrong. Please try again";
"pockets.edgeCase.errors.loadingFailure.actionButton.title" = "Try again";

// MARK: - Categories
"pockets.categories.labels.home" = "Home";
"pockets.categories.labels.travel" = "Travel";
"pockets.categories.labels.rent" = "Rent";
"pockets.categories.labels.holidays" = "Holidays";
"pockets.categories.labels.savings" = "Savings";
"pockets.categories.labels.utilities" = "Utilities";
"pockets.categories.labels.new-car" = "New Car";
"pockets.categories.labels.insurance" = "Insurance";
"pockets.categories.labels.new-phone" = "New Phone";
"pockets.categories.labels.entertainment" = "Entertainment";
"pockets.categories.labels.gift" = "Gift";
"pockets.categories.labels.business" = "Business";

// MARK: - Tutorial
"pockets.tutorial.labels.title" = "Get started with\nPockets!";
"pockets.tutorial.labels.subtitle" = "A personalized space for your money that helps you reach your saving goals even faster.";
"pockets.tutorial.labels.firstItem.title" = "Make it yours";
"pockets.tutorial.labels.firstItem.subtitle" = "Give your pocket its own name and image";
"pockets.tutorial.labels.secondItem.title" = "What's your goal?";
"pockets.tutorial.labels.secondItem.subtitle" = "Set a goal amount and goal date";
"pockets.tutorial.labels.thirdItem.title" = "Transfer money";
"pockets.tutorial.labels.thirdItem.subtitle" = "Easily move money to and from your pocket";
"pockets.tutorial.buttons.nextAction" = "Continue";

// MARK: - Overview
"pockets.overview.labels.title" = "Pockets";
"pockets.overview.edgeCase.labels.emptyList.title" = "Save more with pockets!";
"pockets.overview.edgeCase.labels.emptyList.subtitle" = "Set up a dedicated space to help you stay\non track with your saving goals.";
"pockets.overview.edgeCase.buttons.emptyList.retry" = "Create pocket";

"pockets.overview.cell.labels.progressDescription" = "%@ Completed";
"pockets.overview.cell.labels.goalAmountText" = "/%@";
"pockets.overview.cell.labels.noGoalDescription" = "Set an amount and due date to better keep track of your progress";

// MARK: - Details

"pockets.details.labels.goalAmountText" = "/%@";
"pockets.details.labels.goalDeadlineTitle" = "Goal date: %@";
"pockets.details.labels.progressDescription" = "%@ Completed";
"pockets.details.labels.firstActionItemTitle" = "Add money";
"pockets.details.labels.secondActionItemTitle" = "Withdraw";

// MARK: - Create pocket name and image

"pockets.createPocketImageAndName.labels.title" = "Create pocket";
"pockets.createPocketImageAndName.labels.headerTitle" = "Make it yours";
"pockets.createPocketImageAndName.labels.headerSubtitle" = "Choose your pocket name and image";
"pockets.createPocketImageAndName.labels.pocketNameTitle" = "What's this pocket for?";
"pockets.createPocketImageAndName.labels.subtitle.error" = "You need to specify a pocket name";
"pockets.createPocketImageAndName.buttons.nextAction" = "Continue";

// MARK: - Choose image

"pockets.createPocketChooseImage.labels.title" = "Choose pocket image";

// MARK: - Create pocket goal

"pockets.createPocketGoal.labels.title" = "Create pocket";
"pockets.createPocketGoal.buttons.skip" = "Skip";
"pockets.createPocketGoal.labels.headerTitle" = "What’s your goal?";
"pockets.createPocketGoal.labels.headerSubtitle" = "Choose your goal amount and due date.";
"pockets.createPocketGoal.labels.amount.title" = "Goal amount";
"pockets.createPocketGoal.labels.amount.subtitle.error" = "You need to specify a pocket goal amount";
"pockets.createPocketGoal.buttons.amount.toolbarAction" = "Done";
"pockets.createPocketGoal.labels.deadline.title" = "Set goal date";
"pockets.createPocketGoal.labels.deadline.subtitle" = "It will help you to track progress";
"pockets.createPocketGoal.labels.deadline.dateTitle" = "Ends";
"pockets.createPocketGoal.labels.deadline.emptyDateTitle" = "Select date";
"pockets.createPocketGoal.buttons.nextAction" = "Continue";

// MARK: - Create pocket review

"pockets.createPocketReview.labels.title" = "Review pocket";
"pockets.createPocketReview.labels.goal.deadlineTitle" = "Goal date: ";
"pockets.createPocketReview.labels.goal.noAmountTitle" = "No goal amount";
"pockets.createPocketReview.labels.goal.noDeadlineTitle" = "No goal date";

"pockets.createPocketReview.labels.transferDetails.title" = "TRANSFER DETAILS";
"pockets.createPocketReview.labels.transferDetails.accountTitle" = "From";
"pockets.createPocketReview.labels.transferDetails.amountTitle" = "Amount";

"pockets.createPocketReview.labels.termsAndConditions.explanation" = "By confirming, you agree to the following %@";
"pockets.createPocketReview.labels.termsAndConditions.actionTitle" = "terms and conditions.";

"pockets.createPocketReview.buttons.confirmActionTitle" = "Confirm pocket";
"pockets.createPocketReview.edgeCase.labels.failedToCreatePocket.title" = "Failed to create pocket";
"pockets.createPocketReview.edgeCase.labels.failedToCreatePocket.subtitle" = "Unable to proceed, please try again.";
"pockets.createPocketReview.edgeCase.buttons.failedToCreatePocket.action" = "OK";

// MARK: - Create pocket success
"pockets.createPocketSuccess.edgeCase.labels.title" = "Your pocket is ready!";
"pockets.createPocketSuccess.edgeCase.labels.subtitle" = "Your “%@” pocket is created.\nYou can now start saving!";
// TODO enable following lines when add money is added in the create pocket steps.
//"pockets.createPocketSuccess.edgeCase.labels.addMoneySubtitle" = "Your “%1$@” pocket is created\nand you’ve just added %2$@.";
"pockets.createPocketSuccess.buttons.nextAction" = "View pocket";

// MARK: - Add money
"pockets.addMoney.labels.title" = "Add money";
"pockets.addMoney.labels.accountSection.title" = "From";
"pockets.addMoney.labels.accountSection.placeholder" = "Select an account to transfer from";
"pockets.addMoney.buttons.amount.toolbarAction" = "Done";
"pockets.addMoney.labels.amountSection.title" = "Amount";
"pockets.addMoney.labels.footNote" = "Your transfer will be executed immediately";
"pockets.addMoney.buttons.confirmActionTitle" = "Confirm transfer";

"pockets.addMoney.edgeCase.errors.notConnected.title" = "No internet connection";
"pockets.addMoney.edgeCase.errors.notConnected.subtitle" = "It seems you’re not connected to a network";
"pockets.addMoney.edgeCase.errors.notConnected.actionButton.title" = "OK";

"pockets.addMoney.edgeCase.errors.loadingFailure.title" = "Oops, loading failed";
"pockets.addMoney.edgeCase.errors.loadingFailure.subtitle" = "Something went wrong. Please try again";
"pockets.addMoney.edgeCase.errors.loadingFailure.actionButton.title" = "OK";

"pockets.addMoney.edgeCase.errors.missingAllFields.title" = "Failed to Add Money";
"pockets.addMoney.edgeCase.errors.missingAllFields.subtitle" = "You need to select an account and specify an amount in order to proceed.";
"pockets.addMoney.edgeCase.errors.missingAllFields.actionButton.title" = "OK";

"pockets.addMoney.edgeCase.errors.insufficientBalance.title" = "Failed to Add Money";
"pockets.addMoney.edgeCase.errors.insufficientBalance.subtitle" = "Your transfer amount cannot be higher than the available balance.";
"pockets.addMoney.edgeCase.errors.insufficientBalance.actionButton.title" = "OK";

// MARK: - Add money success

"pockets.addMoneySuccess.edgeCase.labels.title" = "Transfer successful!";
"pockets.addMoneySuccess.edgeCase.labels.subtitle" = "You've just transfered %@ to your '%@' pocket.";
"pockets.addMoneySuccess.buttons.nextAction" = "View pocket";

// MARK: - Withdraw money
"pockets.withdrawMoney.labels.title" = "Withdraw money";
"pockets.withdrawMoney.labels.accountSection.title" = "To";
"pockets.withdrawMoney.labels.accountSection.placeholder" = "Select an account to transfer to";
"pockets.withdrawMoney.buttons.amount.toolbarAction" = "Done";
"pockets.withdrawMoney.labels.amountSection.title" = "Amount";
"pockets.withdrawMoney.labels.amountSection.footNote" = "You have a total of %@ in this pocket.";
"pockets.withdrawMoney.labels.footNote" = "Your transfer will be executed immediately";
"pockets.withdrawMoney.buttons.confirmActionTitle" = "Confirm Withdrawal";

"pockets.withdrawMoney.edgeCase.errors.notConnected.title" = "No internet connection";
"pockets.withdrawMoney.edgeCase.errors.notConnected.subtitle" = "It seems you’re not connected to a network";
"pockets.withdrawMoney.edgeCase.errors.notConnected.actionButton.title" = "OK";

"pockets.withdrawMoney.edgeCase.errors.loadingFailure.title" = "Oops, loading failed";
"pockets.withdrawMoney.edgeCase.errors.loadingFailure.subtitle" = "Something went wrong. Please try again";
"pockets.withdrawMoney.edgeCase.errors.loadingFailure.actionButton.title" = "OK";

"pockets.withdrawMoney.edgeCase.errors.missingAllFields.title" = "Failed to Withdraw Money";
"pockets.withdrawMoney.edgeCase.errors.missingAllFields.subtitle" = "You need to select an account and specify an amount in order to proceed.";
"pockets.withdrawMoney.edgeCase.errors.missingAllFields.actionButton.title" = "OK";

"pockets.withdrawMoney.edgeCase.errors.insufficientBalance.title" = "Failed to Withdraw Money";
"pockets.withdrawMoney.edgeCase.errors.insufficientBalance.subtitle" = "Your transfer amount cannot be higher than the pocket balance.";
"pockets.withdrawMoney.edgeCase.errors.insufficientBalance.actionButton.title" = "OK";

// MARK: - Withdraw money success

"pockets.withdrawMoneySuccess.edgeCase.labels.title" = "Withdrawal Successful!";
"pockets.withdrawMoneySuccess.edgeCase.labels.subtitle" = "You've just withdrawn %@ from your '%@' pocket.";
"pockets.withdrawMoneySuccess.buttons.nextAction" = "View pocket";

// MARK: - Account selector
"pockets.accountSelector.labels.title.from" = "From";
"pockets.accountSelector.labels.title.to" = "To";
"pockets.accountSelector.labels.savingsAccountBalanceTitle" = "Available balance";
"pockets.accountSelector.labels.currentAccountBalanceTitle" = "Available balance";
