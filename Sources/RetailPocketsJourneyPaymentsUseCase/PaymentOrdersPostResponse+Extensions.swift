//
// Created by Backbase R&D B.V. on 03/05/2021.
//

import Foundation
import PaymentOrderClient2
import RetailPocketsJourney
import ClientCommon

extension RetailPocketsJourney.ErrorResponse {
    init(statusCode: Int? = nil, data: Data? = nil, error: CallError) {
        self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
    }

    init(_ errorResponse: ClientCommon.ErrorResponse) {
        switch errorResponse {
        case let .error(statusCode, data, error as ClientCommon.CallError):
            self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
        case let .error(statusCode, data, error):
            self.init(statusCode: statusCode, data: data, error: Self.mapNsError(error: error) ?? error)
        default:
            self.init(statusCode: 0, data: nil, error: NSError())
        }
    }

    private static func mapCommonClientError(_ error: ClientCommon.CallError?) -> Error? {
        switch error {
        case .emptyDataResponse:
            return Pockets.Error.invalidResponse
        case .nilHTTPResponse(let detailError), .unsuccessfulHTTPStatusCode(let detailError):
            return Self.mapNsError(error: detailError)
        default:
            return Pockets.Error.loadingFailure(underlying: nil)
        }
    }

    private static func mapNsError(error: Swift.Error?) -> Pockets.Error? {
        guard let nsError = (error as NSError?),
              nsError.code == NSURLErrorNotConnectedToInternet ||
                nsError.domain == kCFErrorDomainCFNetwork as String
        else {
            return Pockets.Error.loadingFailure(underlying: error)
        }
        return Pockets.Error.notConnected
    }
}

extension RetailPocketsJourney.PaymentOrdersPostResponse {
    init(from paymentOrdersPostResponse: PaymentOrderClient2.PaymentOrdersPostResponse) {
        let rateInfo = RetailPocketsJourney.ExchangeRateInformation.init(from: paymentOrdersPostResponse.exchangeRateInformation)
        self.init(identifier: paymentOrdersPostResponse.id,
                  status: RetailPocketsJourney.Status.init(from: paymentOrdersPostResponse.status),
                  bankStatus: paymentOrdersPostResponse.bankStatus,
                  reasonCode: paymentOrdersPostResponse.reasonCode,
                  reasonText: paymentOrdersPostResponse.reasonText,
                  errorDescription: paymentOrdersPostResponse.errorDescription,
                  nextExecutionDate: paymentOrdersPostResponse.nextExecutionDate,
                  paymentSetupId: paymentOrdersPostResponse.paymentSetupId,
                  paymentSubmissionId: paymentOrdersPostResponse.paymentSubmissionId,
                  approvalStatus: paymentOrdersPostResponse.approvalStatus,
                  transferFee: RetailPocketsJourney.Currency.init(from: paymentOrdersPostResponse.transferFee),
                  exchangeRateInformation: rateInfo,
                  additions: paymentOrdersPostResponse.additions)
    }
}

extension RetailPocketsJourney.Status {
    // swiftlint:disable:next cyclomatic_complexity
    init(from status: PaymentOrderClient2.Status) {
        switch status {
        case .accepted: self = .accepted
        case .draft: self = .draft
        case .entered: self = .entered
        case .ready: self = .ready
        case .processed: self = .processed
        case .rejected: self = .rejected
        case .cancelled: self = .cancelled
        case .cancellationPending: self = .cancellationPending
        case .confirmationPending: self = .confirmationPending
        case .confirmationDeclined: self = .confirmationDeclined
        @unknown default: fatalError()
        }
    }
}

extension RetailPocketsJourney.ExchangeRateInformation {
    init?(from exchangeRateInformation: PaymentOrderClient2.ExchangeRateInformation?) {
        guard let exchangeRateInformation = exchangeRateInformation else { return nil }
        self.init(currencyCode: exchangeRateInformation.currencyCode,
                  rate: exchangeRateInformation.rate,
                  rateType: RetailPocketsJourney.ExchangeRateInformation.RateType.init(from: exchangeRateInformation.rateType),
                  contractIdentification: exchangeRateInformation.contractIdentification)
    }
}

extension RetailPocketsJourney.ExchangeRateInformation.RateType {
    init?(from rateType: PaymentOrderClient2.ExchangeRateInformation.RateType?) {
        guard let rateType = rateType else { return nil }
        switch rateType {
        case .actual: self = .actual
        case .indicative: self = .indicative
        case .agreed: self = .agreed
        @unknown default: fatalError()
        }
    }
}

extension RetailPocketsJourney.Currency {
    init?(from currency: PaymentOrderClient2.Currency?) {
        guard let currency = currency else { return nil }
        self.init(from: currency)
    }

    init(from currency: PaymentOrderClient2.Currency) {
        self.init(amount: currency.amount,
                  currencyCode: currency.currencyCode,
                  additions: currency.additions)
    }
}
