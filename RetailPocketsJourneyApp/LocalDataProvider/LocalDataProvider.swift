//
//  LocalDataProvider.swift
//  RetailPocketsJourney-App
//
//  Created by Backbase on 14/01/2021.
//

import Foundation
import Backbase

class LocalDataProvider: NSObject, DBSDataProvider {
    private enum HTTPMethod: String {
        case POST
        case GET
    }

    override init() {
        super.init()

        initialStubs.forEach(setup)

        let launchEnvironment = ProcessInfo.processInfo.environment
        if let value = launchEnvironment["EmptyList"], value == "true" {
            initialStubs.append(emptyPockets)
        } else {
            initialStubs.append(pocketsList)
        }

        initialStubs.forEach(setup)
    }

    private class StubInfo {
        enum StubResponse {
            case ok(String), notFound, internalServerError

            public var statusCode: Int {
                switch self {
                case .ok: return 200
                case .notFound: return 404
                case .internalServerError: return 500
                }
            }

            var data: Data? {
                switch self {
                case .ok(let path):
                    guard let filePath = Bundle.main.path(forResource: path, ofType: nil) else {
                        fatalError("Failed to find file \(path)")
                    }
                    let fileUrl = URL(fileURLWithPath: filePath)
                    return try? Data(contentsOf: fileUrl, options: .uncached)
                default: return nil
                }
            }
        }

        let path: String
        let params: [String: String]
        let method: HTTPMethod
        let responses: [StubResponse]
        private var resultUsage = 0

        init(path: String, params: [String: String] = [:], method: HTTPMethod, responses: [StubResponse]) {
            self.path = path
            self.params = params
            self.method = method
            self.responses = responses
        }

        func result(for url: URL) -> (URLResponse?, Data?) {
            defer {
                resultUsage += 1
            }
            let requestCount = min(resultUsage, responses.count - 1)
            return (HTTPURLResponse(url: url, statusCode: responses[requestCount].statusCode, httpVersion: nil, headerFields: nil), responses[requestCount].data)
        }

        static func internalServerError(for request: URLRequest)  -> URLResponse? {
            HTTPURLResponse(url: request.url!, statusCode: 500, httpVersion: nil, headerFields: nil)
        }
    }

    private var get: [String: StubInfo] = [:]
    private var post: [String: StubInfo] = [:]

    private let pocketsPath = "/api/pocket-tailor/client-api/v2/pockets"
    private let pocketPath = "/api/pocket-tailor/client-api/v2/pockets/12345"
    private let arrangementsPath = "/api/arrangement-manager/client-api/v2/productsummary/context/arrangements"
    private let paymentsPath = "/api/payment-order-service/client-api/v2/payment-orders"

    private lazy var initialStubs = [StubInfo(path: pocketsPath, method: .POST, responses: [.ok("pocket.json")]),
                                     StubInfo(path: pocketPath, method: .GET, responses: [.ok("pocket.json")]),
                                     StubInfo(path: arrangementsPath, method: .GET, responses: [.ok("arrangements.json")]),
                                     StubInfo(path: paymentsPath, method: .POST, responses: [.ok("payment-order-response.json")])]
    private lazy var emptyPockets = StubInfo(path: pocketsPath, method: .GET, responses: [.ok("empty-pockets.json")])
    private lazy var pocketsList = StubInfo(path: pocketsPath, method: .GET, responses: [.ok("pockets.json")])

    func execute(_ request: URLRequest, completionHandler: ((URLResponse?, Data?, Swift.Error?) -> Void)? = nil) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            guard let unwrappedMethod = request.httpMethod, let method = HTTPMethod.init(rawValue: unwrappedMethod),
                let url = request.url, let components = URLComponents.init(url: url, resolvingAgainstBaseURL: false) else {
                    completionHandler?(StubInfo.internalServerError(for: request), nil, DataProviderError.notSupported)
                    return
            }

            let dict: [String: StubInfo]
            switch method {
            case .GET:
                dict = self.get
            case .POST:
                dict = self.post
            }

            if let stub = dict[components.path] {
                let (response, data) = stub.result(for: url)
                completionHandler?(response, data, nil)
            } else {
                let path = components.path
                let range = NSRange(location: 0, length: path.utf16.count)
                let result: String? = dict.keys.reduce(nil) { (result, candidate) -> String? in
                    if result != nil { return result }
                    let regex = try? NSRegularExpression(pattern: candidate)
                    if regex?.firstMatch(in: path, options: [], range: range) != nil {
                        return candidate
                    }
                    return nil
                }
                if let key = result, let stub = dict[key] {
                    let (response, data) = stub.result(for: url)
                    completionHandler?(response, data, nil)
                }
                completionHandler?(StubInfo.internalServerError(for: request), nil, DataProviderError.notSupported)
            }
        }
    }

    private func setup(_ stub: StubInfo) -> Void {
        switch stub.method {
        case .GET : get[stub.path+getQueryString(stub.params)] = stub
        case .POST: post[stub.path+getQueryString(stub.params)] = stub
        }
    }

    private func getQueryString(_ dict: [String: String]) -> String {
        var output: String = "?"
        let array = dict.sorted { (first, second) -> Bool in
            return (first.key.localizedCaseInsensitiveCompare(second.key) == .orderedAscending)
        }
        array.forEach({ output += "\($0.key)=\($0.value)&" })
        output = String(output.dropLast())
        return output
    }
}

public enum DataProviderError: Error {
    case notSupported
}
