//
// Copyright © 2019 Backbase R&D B.V. All rights reserved.
//

struct TestBehavior: Codable {
    var given: BehaviourConditions!
    var then: [Behaviour] = []
}
