//
// Copyright © 2019 Backbase R&D B.V. All rights reserved.
//

struct BehaviourConditions: Codable {
    var url: String
    var method: HTTPMethod
    var params: [String: String]
}
