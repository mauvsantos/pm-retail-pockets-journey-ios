//
//  Created by Backbase R&D B.V. on 11/03/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
import PocketsClient2
@testable import RetailPocketsJourney

final class CreatePocketChooseImageViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true
        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testDefaultScreenWithoutPreselection() {
        let navigationController = UINavigationController()
        let viewController = CreatePocketChooseImage.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "choose-image", for: navigationController).forEach { XCTFail($0) }
    }

    func testDefaultScreenWithPreselection() {
        let navigationController = UINavigationController()
        let entryParams = CreatePocketChooseImage.EntryParams(selectedImageName: "utilities", didChooseImage: nil)
        let viewController = CreatePocketChooseImage.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "preselected-image", for: navigationController).forEach { XCTFail($0) }
    }

    func testDefaultScreenWithPreselectionAndMultipleLines() {
        var config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
        Resolver.register { config }
        config.defaultPocketItems.insert(PocketConfigurationItem.init(imageName: "custom",
                                                                      image: UIImage.named("ic_gift", in: .pockets),
                                                                      name: "This is a custom and long name"),
                                         at: 0)
        Resolver.register { config }
        let navigationController = UINavigationController()
        let entryParams = CreatePocketChooseImage.EntryParams(selectedImageName: "utilities", didChooseImage: nil)
        let viewController = CreatePocketChooseImage.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "multi-lines-image-name", for: navigationController).forEach { XCTFail($0) }
    }

}

