//
//  Created by Backbase R&D B.V. on 19/02/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
@testable import RetailPocketsJourney

final class CreatePocketSuccessViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testCreatePocketSuccess() {
        let navigationController = UINavigationController()
        let pocket = Pocket(identifier: "1234",
                            arrangementId: "someid",
                            name: "Home",
                            icon: "home",
                            balance: Currency(amount: "0", currencyCode: "USD"))
        let params = CreatePocketSuccess.EntryParams(pocket: pocket)
        let viewController = CreatePocketSuccess.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-success", for: navigationController).forEach { XCTFail($0) }
    }

    // TODO enable following lines when add money is added in the create pocket steps.
//    func testCreatePocketSuccessWithBalance() {
//        let navigationController = UINavigationController()
//        let pocket = Pocket(identifier: "1234",
//                            arrangementId: "someid",
//                            name: "Home",
//                            icon: "home",
//                            balance:Currency(amount: "150", currencyCode: "USD"))
//        let params = CreatePocketSuccess.EntryParams(pocket: pocket)
//        let viewController = CreatePocketSuccess.build(navigationController: navigationController, entryParams: params)
//        navigationController.viewControllers = [viewController]
//        verifySnapshots(withName: "create-pocket-success-with-balance", for: navigationController).forEach { XCTFail($0) }
//    }
}
