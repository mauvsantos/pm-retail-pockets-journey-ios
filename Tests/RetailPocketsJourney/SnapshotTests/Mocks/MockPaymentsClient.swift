//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import PaymentOrderClient2
import ClientCommon

class MockPaymentsClient: PaymentOrdersAPIProtocol {
    enum MockError: Error {
        case generalError
    }

    var postPaymentOrderCallMocks: [MockCall<PaymentOrdersPostResponse>] = []
    var errors: [Error] = []

    func postPaymentOrdersCall(paymentOrdersPost: PaymentOrdersPost,
                               X_MFA: String?) throws -> Call<PaymentOrdersPostResponse> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }
        return postPaymentOrderCallMocks.removeFirst()
    }

    // MARK: - Not implemented

    func deletePaymentOrderByIdCall(paymentOrderId: String, version: Int) throws -> Call<NoResponse> {
        throw MockError.generalError
    }

    func getApprovablePaymentOrdersCall(cursor: String?, size: Int?) throws -> Call<[IdentifiedPaymentOrder]> {
        throw MockError.generalError
    }

    func getCurrenciesCall() throws -> Call<[CurrenciesGetResponseBody]> {
        throw MockError.generalError
    }

    func getPaymentOrderByIdCall(paymentOrderId: String, userId: String?, saId: String?) throws -> Call<PaymentOrderGetResponse> {
        throw MockError.generalError
    }

    func getPaymentOrdersCall(status: [String]?,
                              paymentTypeGroup: String?,
                              createdByMe: Bool?,
                              userId: String?,
                              saId: String?,
                              query: String?,
                              amountFrom: Double?,
                              amountTo: Double?,
                              executionDate: Date?,
                              executionDateFrom: Date?,
                              executionDateTo: Date?,
                              nextExecutionDate: Date?,
                              nextExecutionDateFrom: Date?,
                              nextExecutionDateTo: Date?,
                              paymentMode: PaymentOrdersAPI.PaymentMode_getPaymentOrders?,
                              paymentSetupId: String?,
                              transferFrequency: PaymentOrdersAPI.TransferFrequency_getPaymentOrders?,
                              from: Int?,
                              size: Int?,
                              orderBy: String?,
                              direction: PaymentOrdersAPI.Direction_getPaymentOrders?) throws -> Call<[IdentifiedPaymentOrder]> {
        throw MockError.generalError
    }

    func getProgressStatusByPaymentOrderIdCall(paymentOrderId: String) throws -> Call<PaymentOrderProgressStatusGet> {
        throw MockError.generalError
    }

    func getRateCall(currencyFrom: String, currencyTo: String) throws -> Call<RateGet> {
        throw MockError.generalError
    }

    func postCancelByPaymentOrderIdCall(paymentOrderId: String,
                                        paymentOrderCancelPostRequest: PaymentOrderCancelPostRequest) throws -> Call<PaymentOrderCancelPostResponse> {
        throw MockError.generalError
    }

    func postValidateCall(paymentOrdersValidatePost: PaymentOrdersValidatePost) throws -> Call<PaymentOrdersValidatePostResponse> {
        throw MockError.generalError
    }

    func putApprovalsByPaymentOrderIdCall(paymentOrderId: String,
                                          paymentOrderApprovalPutRequest: PaymentOrderApprovalPutRequest) throws -> Call<PaymentOrdersPostResponse> {
        throw MockError.generalError
    }

    func putBulkApprovalsCall(bulkPaymentOrdersApprovalPutRequest: BulkPaymentOrdersApprovalPutRequest) throws -> Call<BulkPaymentOrdersApprovalPutResponse> {
        throw MockError.generalError
    }

    func putPaymentOrderByIdCall(paymentOrderId: String,
                                 paymentOrderPut: PaymentOrderPut, X_MFA: String?) throws -> Call<PaymentOrdersPostResponse> {
        throw MockError.generalError
    }
}
