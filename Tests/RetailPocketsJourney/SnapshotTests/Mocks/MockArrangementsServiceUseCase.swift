//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon
import ArrangementsClient2
import RetailPocketsJourney
import Resolver

final public class MockArrangementsServiceUseCase: RetailPocketsJourney.ArrangementsServiceUseCase {

    public init() {}

    public func getArrangements(requestParams: GetArrangementsRequestParams,
                                completion: @escaping (Result<[RetailPocketsJourney.ProductSummaryItem],
                                                              RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.getArrangementsByBusinessFunctionCall(businessFunction: "",
                                                             resourceName: "",
                                                             privilege: "",
                                                             contentLanguage: nil,
                                                             withLatestBalances: nil,
                                                             maskIndicator: nil,
                                                             debitAccount: nil,
                                                             creditAccount: nil,
                                                             externalTransferAllowed: nil,
                                                             productKindName: nil,
                                                             legalEntityIds: nil,
                                                             sourceId: nil,
                                                             favorite: nil,
                                                             searchTerm: nil,
                                                             customOrder: nil,
                                                             favoriteFirst: nil,
                                                             from: nil,
                                                             size: nil,
                                                             cursor: nil,
                                                             orderBy: nil,
                                                             direction: nil)
                .execute({ (result) in
                    switch result {
                    case let .success(response):
                        if let body = response.body {
                            let items = body.map(RetailPocketsJourney.ProductSummaryItem.init)
                            completion(.success(items))
                        } else {
                            completion(.failure(RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: Pockets.Error.invalidResponse)))
                        }
                    case let .failure(errorResponse):
                        completion(.failure(RetailPocketsJourney.ErrorResponse(errorResponse)))
                    }
                })

        } catch {
            let errorCode = (error as NSError).code
            let clientErrorResponse = ClientCommon.ErrorResponse.error(errorCode, nil, error)
            completion(.failure(RetailPocketsJourney.ErrorResponse(clientErrorResponse)))
        }
    }

    private lazy var client: ProductSummaryAPIProtocol = {
        if let dbsClient = Backbase.registered(client: ProductSummaryAPI.self),
           let client = dbsClient as? ProductSummaryAPI
        {
            return client
        } else if let client = Resolver.optional(ProductSummaryAPIProtocol.self, name: nil, args: nil) {
            return client
        } else if let client = Resolver.optional(ProductSummaryAPI.self, name: nil, args: nil) {
            return client
        } else {
            guard let serverURL = URL(string: Backbase.configuration().backbase.serverURL) else {
                fatalError("Invalid or no serverURL found in the SDK configuration.")
            }

            let newServerURL = serverURL
                .appendingPathComponent("api")
                .appendingPathComponent("arrangement-manager")

            let client = ProductSummaryAPI()
            client.baseURL = serverURL
            if let dataProvider = Resolver.optional(DBSDataProvider.self) {
                client.dataProvider = dataProvider
                return client
            } else {
                try? Backbase.register(client: client)
                guard let dbsClient = Backbase.registered(client: ProductSummaryAPI.self),
                      let client = dbsClient as? ProductSummaryAPI
                else {
                    fatalError("Failed to retrieve Arrangements client")
                }
                return client
            }
        }
    }()
}

extension RetailPocketsJourney.OrderByField {
    init?(_ field: ArrangementsClient2.OrderByField?) {
        guard let field = field else { return nil }
        self.init(rawValue: field.rawValue)
    }

    var field: ArrangementsClient2.OrderByField? {
        return ArrangementsClient2.OrderByField(rawValue: rawValue)
    }
}

extension RetailPocketsJourney.SortDirection {
    init?(_ direction: ArrangementsClient2.SortDirection?) {
        guard let direction = direction else { return nil }
        self.init(rawValue: direction.rawValue)
    }

    var direction: ArrangementsClient2.SortDirection? {
        return ArrangementsClient2.SortDirection(rawValue: rawValue)
    }
}

extension RetailPocketsJourney.SummaryProductKindItem {
    init?(_ item: ArrangementsClient2.SummaryProductKindItem?) {
        guard let item = item else { return nil }
        self.init(identifier: item.id,
                  externalKindId: item.externalKindId,
                  kindName: item.kindName,
                  kindUri: item.kindUri,
                  additions: item.additions)
    }
}

extension RetailPocketsJourney.DebitCardItem {
    init(_ item: ArrangementsClient2.DebitCardItem) {
        self.init(number: item.number,
                  expiryDate: item.expiryDate,
                  cardId: item.cardId,
                  cardholderName: item.cardholderName,
                  cardType: item.cardType,
                  cardStatus: item.cardStatus,
                  additions: item.additions)
    }
}

extension RetailPocketsJourney.StateItem {
    init?(_ item: ArrangementsClient2.StateItem?) {
        guard let item = item else { return nil }
        self.init(externalStateId: item.externalStateId, state: item.state, additions: item.additions)
    }
}

extension RetailPocketsJourney.TimeUnit {
    init?(_ unit: ArrangementsClient2.TimeUnit?) {
        guard let unit = unit else { return nil }
        self.init(rawValue: unit.rawValue)
    }
}

extension RetailPocketsJourney.UserPreferences {
    init?(_ preferences: ArrangementsClient2.UserPreferences?) {
        guard let preferences = preferences else { return nil }
        self.init(alias: preferences.alias, visible: preferences.visible, favorite: preferences.favorite, additions: preferences.additions)
    }
}

extension RetailPocketsJourney.ProductItem {
    init?(_ item: ArrangementsClient2.ProductItem?) {
        guard let item = item else { return nil }
        self.init(externalId: item.externalId,
                  externalTypeId: item.externalTypeId,
                  typeName: item.typeName,
                  productKind: RetailPocketsJourney.SummaryProductKindItem(item.productKind),
                  additions: item.additions)
    }
}

extension RetailPocketsJourney.ProductSummaryItem {
    init(_ item: ArrangementsClient2.ProductSummaryItem) {
        self.init(identifier: item.id,
                  externalArrangementId: item.externalArrangementId,
                  externalLegalEntityId: item.externalLegalEntityId,
                  externalProductId: item.externalProductId,
                  name: item.name,
                  bankAlias: item.bankAlias,
                  sourceId: item.sourceId,
                  bookedBalance: item.bookedBalance,
                  availableBalance: item.availableBalance,
                  creditLimit: item.creditLimit,
                  IBAN: item.IBAN,
                  BBAN: item.BBAN,
                  currency: item.currency,
                  externalTransferAllowed: item.externalTransferAllowed,
                  urgentTransferAllowed: item.urgentTransferAllowed,
                  accruedInterest: item.accruedInterest,
                  number: item.number,
                  principalAmount: item.principalAmount,
                  currentInvestmentValue: item.currentInvestmentValue,
                  legalEntityIds: item.legalEntityIds,
                  productId: item.productId,
                  productNumber: item.productNumber,
                  productKindName: item.productKindName,
                  productTypeName: item.productTypeName,
                  BIC: item.BIC,
                  bankBranchCode: item.bankBranchCode,
                  accountOpeningDate: item.accountOpeningDate,
                  accountInterestRate: item.accountInterestRate,
                  valueDateBalance: item.valueDateBalance,
                  creditLimitUsage: item.creditLimitUsage,
                  creditLimitInterestRate: item.creditLimitInterestRate,
                  creditLimitExpiryDate: item.creditLimitExpiryDate,
                  startDate: item.startDate,
                  termUnit: RetailPocketsJourney.TimeUnit(item.termUnit),
                  termNumber: item.termNumber,
                  interestPaymentFrequencyUnit: RetailPocketsJourney.TimeUnit(item.interestPaymentFrequencyUnit),
                  interestPaymentFrequencyNumber: item.interestPaymentFrequencyNumber,
                  maturityDate: item.maturityDate,
                  maturityAmount: item.maturityAmount,
                  autoRenewalIndicator: item.autoRenewalIndicator,
                  interestSettlementAccount: item.interestSettlementAccount,
                  outstandingPrincipalAmount: item.outstandingPrincipalAmount,
                  monthlyInstalmentAmount: item.monthlyInstalmentAmount,
                  amountInArrear: item.amountInArrear,
                  minimumRequiredBalance: item.minimumRequiredBalance,
                  creditCardAccountNumber: item.creditCardAccountNumber,
                  validThru: item.validThru,
                  applicableInterestRate: item.applicableInterestRate,
                  remainingCredit: item.remainingCredit,
                  outstandingPayment: item.outstandingPayment,
                  minimumPayment: item.minimumPayment,
                  minimumPaymentDueDate: item.minimumPaymentDueDate,
                  totalInvestmentValue: item.totalInvestmentValue,
                  debitCards: item.debitCards.map(RetailPocketsJourney.DebitCardItem.init),
                  accountHolderAddressLine1: item.accountHolderAddressLine1,
                  accountHolderAddressLine2: item.accountHolderAddressLine2,
                  accountHolderStreetName: item.accountHolderStreetName,
                  town: item.town,
                  postCode: item.postCode,
                  countrySubDivision: item.countrySubDivision,
                  accountHolderNames: item.accountHolderNames,
                  accountHolderCountry: item.accountHolderCountry,
                  creditAccount: item.creditAccount,
                  debitAccount: item.debitAccount,
                  lastUpdateDate: item.lastUpdateDate,
                  userPreferences: RetailPocketsJourney.UserPreferences(item.userPreferences),
                  product: RetailPocketsJourney.ProductItem(item.product),
                  state: RetailPocketsJourney.StateItem(item.state),
                  parentId: item.parentId,
                  externalParentId: item.externalParentId,
                  financialInstitutionId: item.financialInstitutionId,
                  lastSyncDate: item.lastSyncDate,
                  additions: item.additions)
    }
}
