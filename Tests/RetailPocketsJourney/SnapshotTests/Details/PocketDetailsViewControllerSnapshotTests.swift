import Backbase
import Resolver
import SnapshotTesting
import XCTest
import PocketsClient2
@testable import RetailPocketsJourney

final class PocketDetailsViewControllerSnapshotTests: SnapshotTestCase {
    var client: MockPocketsClient!

    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        do {
            try Backbase.initialize("config-stable.json", forceDecryption: false)
        } catch {
            XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
        }

        client = MockPocketsClient()
        Resolver.register { self.client as PocketTailorClientAPIProtocol }
        Resolver.register { MockPocketsServiceUseCase() as PocketsServiceUseCase }
        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testFullDetailsScreen() {
        let dec2030 = Date(timeIntervalSinceReferenceDate: 60 * 60 * 24 * 365 * 30) // Dec 25, 2030
        let pocket = RetailPocketsJourney.Pocket.init(
            identifier: "0", arrangementId: "", name: "Amsterdam Trip", icon: "holidays",
            goal: PocketGoal.init(amountCurrency: Currency.init(amount: "1500", currencyCode: "USD"),
                                  deadline: dec2030, progress: 75),
            balance: Currency.init(amount: "1125", currencyCode: "USD"))

        let navigationController = UINavigationController()
        let params = Details.EntryParams(pocket: pocket)
        let viewController = Details.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "pocket-details-full", for: navigationController).forEach { XCTFail($0) }
    }

    func testNoDetailsScreen() {
        let pocket = RetailPocketsJourney.Pocket.init(
            identifier: "0", arrangementId: "", name: "Amsterdam Trip", icon: "holidays",
            goal: nil,
            balance: Currency.init(amount: "1125", currencyCode: "EUR"))

        let navigationController = UINavigationController()
        let params = Details.EntryParams(pocket: pocket)
        let viewController = Details.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "pocket-details-empty", for: navigationController).forEach { XCTFail($0) }
    }

//    func testRequestFailureScreen() {
//        client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 500,
//                                                                            header: [:],
//                                                                            body: nil)))]
//        let navigationController = UINavigationController()
//        let viewController = Overview.build(navigationController: navigationController)
//        navigationController.viewControllers = [viewController]
//        verifySnapshots(withName: "overview-failure", for: navigationController).forEach { XCTFail($0) }
//    }
//
//    func testNoInternetConnectionScreen() {
//        let error = NSError(domain: NSURLErrorDomain as String, code: NSURLErrorNotConnectedToInternet, userInfo: nil) as Error
//        client.listPocketsCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, error)))]
//
//        let navigationController = UINavigationController()
//        let viewController = Overview.build(navigationController: navigationController)
//        navigationController.viewControllers = [viewController]
//
//        verifySnapshots(withName: "overview-not-connected", for: viewController).forEach { XCTFail($0) }
//    }
}
