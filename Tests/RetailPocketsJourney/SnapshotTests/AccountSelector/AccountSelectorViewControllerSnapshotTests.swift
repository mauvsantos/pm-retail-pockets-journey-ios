//
//  Created by Backbase R&D B.V. on 11/03/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
import ClientCommon
import ArrangementsClient2
@testable import RetailPocketsJourney

final class AccountSelectorViewControllerSnapshotTests: SnapshotTestCase {
    var client = MockArrangementsClient()
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true
        do {
            try Backbase.initialize("config-stable.json", forceDecryption: false)
        } catch {
            XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
        }
        
        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
        Resolver.register { MockArrangementsServiceUseCase() as ArrangementsServiceUseCase }
        Resolver.register { self.client as ProductSummaryAPIProtocol }
    }

    func testFromWithoutPreselectedAccount() {
        client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: self.getAccounts())))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "from-account", for: navigationController).forEach { XCTFail($0) }
    }

    func testToWithoutPreselectedAccount() {
        client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: self.getAccounts())))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .toAccount,
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "to-account", for: navigationController).forEach { XCTFail($0) }
    }

    func testFromWithPreselectedAccount() {
        client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: self.getAccounts())))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                      accountId: "someid",
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "preselected-from-account", for: navigationController).forEach { XCTFail($0) }
    }

    func testAccountsLoadingFailed() {
        let error = NSError(domain: "", code: 403, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(ClientCommon.ErrorResponse.error(403, nil, error)))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "from-account-loading-failed", for: navigationController).forEach { XCTFail($0) }
    }

    func testAccountsNoInternet() {
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(ClientCommon.ErrorResponse.error(NSURLErrorNotConnectedToInternet, nil, error)))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "from-account-no-internet", for: navigationController).forEach { XCTFail($0) }
    }

    func testAccountsEmpty() {
        client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: [])))]
        let navigationController = UINavigationController()

        let entryParams = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                      didSelectAccount: { _ in })
        let viewController = AccountSelector.build(navigationController: navigationController, entryParams: entryParams)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "from-account-empty-list", for: navigationController).forEach { XCTFail($0) }
    }
}

private extension AccountSelectorViewControllerSnapshotTests {
    func getAccounts() -> [ArrangementsClient2.ProductSummaryItem] {
        let item1 = ArrangementsClient2.ProductSummaryItem(id: "someid",
                                                           name: "My current account",
                                                           availableBalance: 123456.14,
                                                           BBAN: "12345678901234",
                                                           currency: "USD",
                                                           legalEntityIds: [],
                                                           productKindName: "Current Account",
                                                           debitCards: [])
        let item2 = ArrangementsClient2.ProductSummaryItem(id: "someid2",
                                                           name: "My savings account",
                                                           availableBalance: 9991935.1234,
                                                           BBAN: "123456789",
                                                           currency: "USD",
                                                           legalEntityIds: [],
                                                           productKindName: "Savings Account",
                                                           debitCards: [])
        let item3 = ArrangementsClient2.ProductSummaryItem(id: "someid3",
                                                           name: "My current account 2",
                                                           availableBalance: 9991935.1234,
                                                           BBAN: "123456789",
                                                           currency: "USD",
                                                           legalEntityIds: [],
                                                           productKindName: "Savings Account",
                                                           debitCards: [])
        return [item1, item2, item3]
    }
}

