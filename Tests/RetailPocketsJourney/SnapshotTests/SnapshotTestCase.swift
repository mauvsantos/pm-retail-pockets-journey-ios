//
//  Created by Backbase R&D B.V. on 28/7/2020.
//

import XCTest
import SnapshotTesting

class SnapshotTestCase: XCTestCase {
    final func verifySnapshots(withName name: String, for controller: UIViewController, file: StaticString = #file) -> [String] {

        UIView.setAnimationsEnabled(false)

        // finish appearanceTransition before assertions
        controller.beginAppearanceTransition(true, animated: false)
        controller.endAppearanceTransition()

        var  devices: [(ViewImageConfig, String)] = [
            (.iPhone8(.portrait, .light), "iphone8-light"),
            (.iPhoneX(.portrait, .light), "iphone-x-light"),
            (.iPhoneXsMax(.portrait, .light), "iphone-xs-max-light")
        ]

        if #available(iOS 13.0, *) {
            devices += [
                (.iPhone8(.portrait, .dark), "iphone8-dark"),
                (.iPhoneX(.portrait, .dark), "iphone-x-dark"),
                (.iPhoneXsMax(.portrait, .dark), "iphone-xs-max-dark")
            ]
        }

        return devices.compactMap { verifySnapshot(matching: controller, as: .wait(for: 0.1, on: .image(on: $0.0)), named: "\($0.1)-\(osVersion)", file: file, testName: name) }
    }

    private lazy var osVersion: String = {
        let os: String
        #if os(OSX)
        os = "macOS"
        #elseif os(watchOS)
        os = "watchOS"
        #elseif os(tvOS)
        os = "tvOS"
        #elseif os(iOS)
        #if targetEnvironment(macCatalyst)
        os = "macOS - Catalyst"
        #else
        os = "iOS"
        #endif
        #endif

        return os+"\(ProcessInfo().operatingSystemVersion.majorVersion)"
    }()
}

extension UITraitCollection {
    static func iPhone8(_ orientation: ViewImageConfig.Orientation, _ style: ViewImageConfig.InterfaceStyle)
        -> UITraitCollection {
            let userInterfaceStyle: UIUserInterfaceStyle
            switch style {
            case .light:
                userInterfaceStyle = .light
            case .dark:
                userInterfaceStyle = .dark
            }
            let traitCollection = UITraitCollection(userInterfaceStyle: userInterfaceStyle)
            return UITraitCollection(traitsFrom: [iPhone8(orientation), traitCollection])
    }

    static func iPhoneX(_ orientation: ViewImageConfig.Orientation, _ style: ViewImageConfig.InterfaceStyle)
        -> UITraitCollection {
            let userInterfaceStyle: UIUserInterfaceStyle
            switch style {
            case .light:
                userInterfaceStyle = .light
            case .dark:
                userInterfaceStyle = .dark
            }
            let traitCollection = UITraitCollection(userInterfaceStyle: userInterfaceStyle)
            return UITraitCollection(traitsFrom: [iPhoneX(orientation), traitCollection])
    }

    static func iPhoneXsMax(_ orientation: ViewImageConfig.Orientation, _ style: ViewImageConfig.InterfaceStyle)
        -> UITraitCollection {
            let userInterfaceStyle: UIUserInterfaceStyle
            switch style {
            case .light:
                userInterfaceStyle = .light
            case .dark:
                userInterfaceStyle = .dark
            }
            let traitCollection = UITraitCollection(userInterfaceStyle: userInterfaceStyle)
            return UITraitCollection(traitsFrom: [iPhoneXsMax(orientation), traitCollection])
    }
}

extension ViewImageConfig {
    enum InterfaceStyle {
      case light
      case dark

      @available(iOS 12.0, *) var userInterfaceStyle: UIUserInterfaceStyle {
        switch self {
          case .light: return .light
          case .dark: return .dark
        }
      }
    }

    static func iPhone8(_ orientation: Orientation, _ style: InterfaceStyle = .light) -> ViewImageConfig {
        let config = iPhone8(orientation)
        return ViewImageConfig(safeArea: config.safeArea, size: config.size, traits: .iPhone8(orientation, style))
    }

    static func iPhoneX(_ orientation: Orientation, _ style: InterfaceStyle = .light) -> ViewImageConfig {
        let config = iPhoneX(orientation)
        return ViewImageConfig(safeArea: config.safeArea, size: config.size, traits: .iPhoneX(orientation, style))
    }

    static func iPhoneXsMax(_ orientation: Orientation, _ style: InterfaceStyle = .light) -> ViewImageConfig {
        let config = iPhoneXsMax(orientation)
        return ViewImageConfig(safeArea: config.safeArea, size: config.size, traits: .iPhoneXsMax(orientation, style))
    }
}
