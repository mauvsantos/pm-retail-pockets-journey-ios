//
//  Created by Backbase R&D B.V. on 19/02/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
import PocketsClient2
@testable import RetailPocketsJourney

final class CreatePocketImageAndNameViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testDefaultScreen() {
        let navigationController = UINavigationController()
        let viewController = CreatePocketImageAndName.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-name", for: navigationController).forEach { XCTFail($0) }
    }
}
