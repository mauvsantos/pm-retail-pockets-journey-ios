//
//  Created by Backbase R&D B.V. on 11/03/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
import ClientCommon
import PaymentOrderClient2
@testable import RetailPocketsJourney

final class AddMoneyViewControllerSnapshotTests: SnapshotTestCase {
    var client = MockPaymentsClient()
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true
        do {
            try Backbase.initialize("config-stable.json", forceDecryption: false)
        } catch {
            XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
        }
        
        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
        Resolver.register { MockPaymentsServiceUseCase() as PaymentsServiceUseCase }
        Resolver.register { self.client as PaymentOrdersAPIProtocol }
    }

    func testEmptyAddMoney() {
        let response = PaymentOrderClient2.PaymentOrdersPostResponse(id: "123", status: .entered)
        client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: response)))]
        let navigationController = UINavigationController()

        let balance = RetailPocketsJourney.Currency(amount: "123", currencyCode: "USD")
        let pocket = RetailPocketsJourney.Pocket(identifier: "123", arrangementId: "234", name: "Home", icon: "home", balance: balance)
        let entryParams = AddMoney.EntryParams(pocket: pocket)
        let viewController = AddMoney.build(navigationController: navigationController, entryParams: entryParams)
        verifySnapshots(withName: "empty", for: viewController).forEach { XCTFail($0) }
    }
}
