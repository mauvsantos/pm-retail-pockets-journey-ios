import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import PocketsClient2
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

final class PocketDetailsViewModelTests: QuickSpec {

    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var client: MockPocketsClient!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)
            client = MockPocketsClient()

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }

            Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                                 presentableDescription: "$")) }
            Resolver.register { MockPocketsServiceUseCase() as PocketsServiceUseCase }
            Resolver.register { client as PocketTailorClientAPIProtocol }
        }

        afterEach {
            Resolver.reset()
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        }

        describe("ViewModel") {

            context("Details") {

                it("givenViewModel_whenEntryParamsPassed_thenItShouldHaveTitle") {
                    let title = "Amsterdam Trip"
                    let pocket = Pocket(identifier: "", arrangementId: "", name: title, icon: "",
                                        balance: Currency(amount: "1000", currencyCode: "USD"))
                    let entryParams: Details.EntryParams = .init(pocket: pocket)
                    let viewModel = PocketDetailsViewModel(entryParams: entryParams)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable())

                    expect(output.title)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil), .next(10, "Amsterdam Trip")]))
                }

                it("givenViewModel_whenPocketDetailsEntryParamsPassed_thenItShouldHaveHeaderOutput") {
                    let title = "Amsterdam Trip"
                    let pocket = Pocket(identifier: "", arrangementId: "", name: title, icon: "travel",
                                        balance: Currency(amount: "1000", currencyCode: "USD"))
                    let entryParams: Details.EntryParams = .init(pocket: pocket)
                    let viewModel = PocketDetailsViewModel(entryParams: entryParams)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable())

                    let header = PocketDetailsHeaderModel(pocket: pocket)

                    expect(output.header)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil), .next(10, header)]))

                    let headerDataModel = try! output.header.asObservable().take(1).toBlocking().first()
                    expect(headerDataModel).notTo(beNil())
                    expect(headerDataModel??.pocketIconImage).notTo(beNil())
                    expect(headerDataModel??.pocketTitle).to(equal(title))
                    expect(headerDataModel??.currentAmountFormatterData).to(equal("$1,000.00"))
                    expect(headerDataModel??.goalProgressState).to(equal(PocketGoalProgressState.hidden))
                }

                it("givenViewModel_whenPocketDetailsEntryParamsPassedWithPocketId_thenItShouldRetrieveThePocketDetails") {
                    let title = "Amsterdam Trip"
                    let pocket = PocketsClient2.Pocket(id: "someid",
                                                       arrangementId: "somearrangementid",
                                                       name: title,
                                                       icon: "travel",
                                                       balance: Currency(amount: "1000", currencyCode: "USD"))
                    let journeyPocket = RetailPocketsJourney.Pocket(pocket)

                    client.viewPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                       header: [:],
                                                                                       body: pocket)))]

                    let entryParams: Details.EntryParams = .init(pocketId: "someid")
                    let viewModel = PocketDetailsViewModel(entryParams: entryParams)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable())

                    let header = PocketDetailsHeaderModel(pocket: journeyPocket)

                    expect(output.header)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil), .next(10, header)]))

                    let headerDataModel = try! output.header.asObservable().take(1).toBlocking().first()
                    expect(headerDataModel).notTo(beNil())
                    expect(headerDataModel??.pocketIconImage).notTo(beNil())
                    expect(headerDataModel??.pocketTitle).to(equal(title))
                    expect(headerDataModel??.currentAmountFormatterData).to(equal("$1,000.00"))
                    expect(headerDataModel??.goalProgressState).to(equal(PocketGoalProgressState.hidden))
                }

                it("givenViewModel_whenPocketDetailsEntryParamsPassedWithPocketId_thenItShouldReturnRightLoadingState") {
                    let title = "Amsterdam Trip"
                    let pocket = PocketsClient2.Pocket(id: "someid",
                                                       arrangementId: "somearrangementid",
                                                       name: title,
                                                       icon: "travel",
                                                       balance: Currency(amount: "1000", currencyCode: "USD"))

                    client.viewPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                       header: [:],
                                                                                       body: pocket)))]

                    let entryParams: Details.EntryParams = .init(pocketId: "someid")
                    let viewModel = PocketDetailsViewModel(entryParams: entryParams)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable())

                    expect(output.isLoading)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false), .next(10, true), .next(10, false)]))
                }
            }
        }
    }
}
