//
//  Created by Backbase R&D B.V. on 17/02/2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

class CreatePocketImageAndNameViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("CreatePocketImageAndName") {

                it("givenViewModel_whenBindCalled_thenItShouldNOTReturnErrorByDefault") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    let output = viewModel.bind(pocketNameInput: .never(),
                                                continueTap: .never(),
                                                chooseImageTap: .never())
                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil)]))
                }

                it("givenViewModel_whenBindCalled_thenItShouldLoadTheDefaultTextInputInfo") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    let output = viewModel.bind(pocketNameInput: .never(),
                                                continueTap: .never(),
                                                chooseImageTap: .never())
                    expect(output.pocketItem)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, config.defaultPocketItems.first)]))
                }

                it("givenViewModel_whenBindCalledWithContinueTapWithDefaultValue_thenItShouldNOTReturnError") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    let continueTap = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(pocketNameInput: .never(),
                                                continueTap: continueTap.asObservable(),
                                                chooseImageTap: .never())
                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil)]))
                }

                it("givenViewModel_whenBindCalledWithContinueTapWithEmptyPocketName_thenItShouldReturnError") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    let pocketNameChange = scheduler.createColdObservable([.next(10, "")])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let output = viewModel.bind(pocketNameInput: pocketNameChange.asObservable(),
                                                continueTap: continueTap.asObservable(),
                                                chooseImageTap: .never())
                    scheduler.advanceTo(30)
                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, "You need to specify a pocket name")]))
                }

                it("givenViewModel_whenBindCalledWithContinueTapWithNewPocketName_thenItShouldNOTReturnError") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    let pocketNameChange = scheduler.createColdObservable([.next(10, "New name")])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let output = viewModel.bind(pocketNameInput: pocketNameChange.asObservable(),
                                                continueTap: continueTap.asObservable(),
                                                chooseImageTap: .never())
                    scheduler.advanceTo(30)
                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, nil)]))
                }

                it("givenViewModel_whenBindCalledWithChoseImageTap_thenItShouldTriggerChooseImageCallback") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    var imageName: String?
                    viewModel.didSelectChooseImage = { imageName = $0?.selectedImageName }

                    let chooseImageTap = scheduler.createColdObservable([.next(10, ())])

                    let _ = viewModel.bind(pocketNameInput: .never(),
                                           continueTap: .never(),
                                           chooseImageTap: chooseImageTap.asObservable())
                    scheduler.advanceTo(20)
                    expect(imageName).to(equal("home"))
                }

                it("givenViewModel_whenNewImageIsSelected_thenItShouldTriggerCallbackForNewImageSelection") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    var entryParams: CreatePocketChooseImage.EntryParams?
                    viewModel.didSelectChooseImage = { item in
                        entryParams = item
                    }

                    let chooseImageTap = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(pocketNameInput: .never(),
                                           continueTap: .never(),
                                           chooseImageTap: chooseImageTap.asObservable())

                    scheduler.advanceTo(20)

                    let selectedPocket = PocketConfigurationItem(imageName: "utilities",
                                                                 image: UIImage.named("ic_utilities", in: .design),
                                                                 name: "Utilities")
                    entryParams?.didChooseImage?(selectedPocket)

                    expect(output.selectedPocketName)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(20, "Utilities")]))
                }

                it("givenViewModel_whenThereIsErrorForNoImageSelected_andNewImageIsSelected_thenItShouldNOTReturnError") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    var entryParams: CreatePocketChooseImage.EntryParams?
                    viewModel.didSelectChooseImage = { item in
                        entryParams = item
                    }

                    let pocketNameChange = scheduler.createColdObservable([.next(10, "")])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])
                    let chooseImageTap = scheduler.createColdObservable([.next(40, ())])

                    let output = viewModel.bind(pocketNameInput: pocketNameChange.asObservable(),
                                                continueTap: continueTap.asObservable(),
                                                chooseImageTap: chooseImageTap.asObservable())

                    scheduler.advanceTo(30)
                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, "You need to specify a pocket name")]))

                    scheduler.advanceTo(40)
                    let selectedPocket = PocketConfigurationItem(imageName: "utilities",
                                                                 image: UIImage.named("ic_utilities", in: .design),
                                                                 name: "Utilities")
                    entryParams?.didChooseImage?(selectedPocket)

                    expect(output.selectedPocketName)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(40, "Utilities")]))

                    expect(output.errorInfo)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(40, nil)]))
                }

                it("givenViewModel_whenACustomNameEntered_andNewImageIsSelected_thenItShouldNOTReplaceTheCustomPocketName") {
                    let viewModel = CreatePocketImageAndNameViewModel()
                    var entryParams: CreatePocketChooseImage.EntryParams?
                    viewModel.didSelectChooseImage = { item in
                        entryParams = item
                    }

                    var pocketName: String?
                    viewModel.didSelectContinue = { item in
                        pocketName = item.createPocketPostRequestParams?.name
                    }

                    let pocketNameChange = scheduler.createColdObservable([.next(10, "Custom name")])
                    let chooseImageTap = scheduler.createColdObservable([.next(20, ())])
                    let continueTap = scheduler.createColdObservable([.next(40, ())])

                    let _ = viewModel.bind(pocketNameInput: pocketNameChange.asObservable(),
                                           continueTap: continueTap.asObservable(),
                                           chooseImageTap: chooseImageTap.asObservable())

                    scheduler.advanceTo(30)

                    let selectedPocket = PocketConfigurationItem(imageName: "utilities",
                                                                 image: UIImage.named("ic_utilities", in: .design),
                                                                 name: "Utilities")
                    entryParams?.didChooseImage?(selectedPocket)

                    scheduler.advanceTo(50)

                    expect(pocketName).to(equal("Custom name"))
                }
            }
        }
    }
}
