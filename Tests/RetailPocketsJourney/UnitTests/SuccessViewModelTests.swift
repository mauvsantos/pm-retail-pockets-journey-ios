//
//  Created by Backbase R&D B.V. on 17/02/2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

class SuccessViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("CreatePocketGoal") {

                it("givenViewModel_whenAPocketIsReceivedWithoutBalanceInfo_thenItShouldReturnDefaultEdgeCaseItem") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let entryParams = CreatePocketSuccess.EntryParams(pocket: pocket)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.createPocketSuccess.design.successIcon,
                                                                     title: config.createPocketSuccess.strings.title(),
                                                                     subtitle: config.createPocketSuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.createPocketSuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.createPocketSuccess.strings.nextActionTitle(),
                                          style: config.createPocketSuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    let output = viewModel.bind(continueTap: .never())

                    let edgeCaseItem = try! output.edgeCase.asObservable().take(1).toBlocking().first()

                    XCTAssertEqual(edgeCaseItem?.title, "Your pocket is ready!")
                    XCTAssertEqual(edgeCaseItem?.subtitle, "Your “Home” pocket is created.\nYou can now start saving!")
                    XCTAssertNotNil(edgeCaseItem?.icon)
                    XCTAssertNil(edgeCaseItem?.actionButtonTitle)
                }
                // TODO enable following lines when add money is added in the create pocket steps.
//                it("givenViewModel_whenAPocketIsReceivedWithBalanceInfo_thenItShouldReturnDefaultEdgeCaseItem") {
//                    let pocket = Pocket(identifier: "1234", arrangementId: "someid", name: "Home", icon: "home", balance: Currency(amount: "90", currencyCode: "USD"))
//                    let entryParams = CreatePocketSuccess.EntryParams(pocket: pocket)
//                    let viewModel = CreatePocketSuccessViewModel(entryParams: entryParams)
//
//                    let edgeCase = viewModel.bind(continueTap: .never())
//
//                    let edgeCaseItem = try! edgeCase.asObservable().take(1).toBlocking().first()
//
//                    XCTAssertEqual(edgeCaseItem?.title, "Your pocket is ready!")
//                    XCTAssertEqual(edgeCaseItem?.subtitle, "Your “Home” pocket is created\nand you’ve just added $90.")
//                    XCTAssertNotNil(edgeCaseItem?.icon)
//                    XCTAssertNil(edgeCaseItem?.actionButtonTitle)
//                }

                it("givenViewModel_whenAPocketIsReceived_thenItShouldTriggerDidSelectContinue") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let entryParams = CreatePocketSuccess.EntryParams(pocket: pocket)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.createPocketSuccess.design.successIcon,
                                                                     title: config.createPocketSuccess.strings.title(),
                                                                     subtitle: config.createPocketSuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.createPocketSuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.createPocketSuccess.strings.nextActionTitle(),
                                          style: config.createPocketSuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    var didTriggerContinue: Bool = false
                    viewModel.didSelectContinue = {
                        didTriggerContinue = true
                    }

                    let continueTap = scheduler.createColdObservable([.next(20, ())])
                    let _ = viewModel.bind(continueTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    XCTAssertTrue(didTriggerContinue)
                }
            }

            context("AddMoney") {

                it("givenViewModel_whenAPocketIsReceivedWithoutBalanceInfo_thenItShouldReturnDefaultEdgeCaseItem") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let amount = Currency(amount: "123", currencyCode: "USD")
                    let entryParams = AddMoneySuccess.EntryParams(pocket: pocket, amount: amount)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.addMoneySuccess.design.successIcon,
                                                                     title: config.addMoneySuccess.strings.title(),
                                                                     subtitle: config.addMoneySuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.addMoneySuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.addMoneySuccess.strings.nextActionTitle(),
                                          style: config.addMoneySuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    let output = viewModel.bind(continueTap: .never())

                    let edgeCaseItem = try! output.edgeCase.asObservable().take(1).toBlocking().first()

                    XCTAssertEqual(edgeCaseItem?.title, "Transfer successful!")
                    XCTAssertEqual(edgeCaseItem?.subtitle, "You\'ve just transfered $123.00 to your \'Home\' pocket.")
                    XCTAssertNotNil(edgeCaseItem?.icon)
                    XCTAssertNil(edgeCaseItem?.actionButtonTitle)
                }

                it("givenViewModel_whenAPocketIsReceived_thenItShouldTriggerDidSelectContinue") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let amount = Currency(amount: "123", currencyCode: "USD")
                    let entryParams = AddMoneySuccess.EntryParams(pocket: pocket, amount: amount)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.addMoneySuccess.design.successIcon,
                                                                     title: config.addMoneySuccess.strings.title(),
                                                                     subtitle: config.addMoneySuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.addMoneySuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.addMoneySuccess.strings.nextActionTitle(),
                                          style: config.addMoneySuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    var didTriggerContinue: Bool = false
                    viewModel.didSelectContinue = {
                        didTriggerContinue = true
                    }

                    let continueTap = scheduler.createColdObservable([.next(20, ())])
                    let _ = viewModel.bind(continueTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    XCTAssertTrue(didTriggerContinue)
                }
            }

            context("WithdrawMoney") {

                it("givenViewModel_whenAPocketIsReceivedWithoutBalanceInfo_thenItShouldReturnDefaultEdgeCaseItem") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let amount = Currency(amount: "123", currencyCode: "USD")
                    let entryParams = WithdrawMoneySuccess.EntryParams(pocket: pocket, amount: amount)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.withdrawMoneySuccess.design.successIcon,
                                                                     title: config.withdrawMoneySuccess.strings.title(),
                                                                     subtitle: config.withdrawMoneySuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.addMoneySuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.withdrawMoneySuccess.strings.nextActionTitle(),
                                          style: config.withdrawMoneySuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    let output = viewModel.bind(continueTap: .never())

                    let edgeCaseItem = try! output.edgeCase.asObservable().take(1).toBlocking().first()

                    XCTAssertEqual(edgeCaseItem?.title, "Withdrawal Successful!")
                    XCTAssertEqual(edgeCaseItem?.subtitle, "You\'ve just withdrawn $123.00 from your \'Home\' pocket.")
                    XCTAssertNotNil(edgeCaseItem?.icon)
                    XCTAssertNil(edgeCaseItem?.actionButtonTitle)
                }

                it("givenViewModel_whenAPocketIsReceived_thenItShouldTriggerDidSelectContinue") {
                    let pocket = Pocket(identifier: "1234",
                                        arrangementId: "someid",
                                        name: "Home",
                                        icon: "home",
                                        balance: Currency(amount: "0", currencyCode: "USD"))
                    let amount = Currency(amount: "123", currencyCode: "USD")
                    let entryParams = WithdrawMoneySuccess.EntryParams(pocket: pocket, amount: amount)

                    let edgeCaseInfo = SuccessViewModel.EdgeCaseInfo(icon: config.withdrawMoneySuccess.design.successIcon,
                                                                     title: config.withdrawMoneySuccess.strings.title(),
                                                                     subtitle: config.withdrawMoneySuccess.uiDataMapper.subtitle(entryParams),
                                                                     style: config.withdrawMoneySuccess.design.styles.edgeCase)
                    let nextActionInfo = (title: config.withdrawMoneySuccess.strings.nextActionTitle(),
                                          style: config.withdrawMoneySuccess.design.styles.nextActionButton)
                    let successInfo = SuccessViewModel.SuccessInfo(edgeCaseInfo: edgeCaseInfo, nextActionInfo: nextActionInfo)
                    let viewModel = SuccessViewModel(successInfo: successInfo)

                    var didTriggerContinue: Bool = false
                    viewModel.didSelectContinue = {
                        didTriggerContinue = true
                    }

                    let continueTap = scheduler.createColdObservable([.next(20, ())])
                    let _ = viewModel.bind(continueTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    XCTAssertTrue(didTriggerContinue)
                }
            }
        }
    }
}
