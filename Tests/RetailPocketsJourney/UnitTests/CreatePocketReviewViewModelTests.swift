//
//  Created by Backbase R&D B.V. on 17/02/2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign
import PocketsClient2

@testable import RetailPocketsJourney

class CreatePocketReviewViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!
        var client: MockPocketsClient!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)
            client = MockPocketsClient()

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }
            Resolver.register { MockPocketsServiceUseCase() as PocketsServiceUseCase }
            Resolver.register { client as PocketTailorClientAPIProtocol }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("CreatePocketReview") {

                it("givenViewModel_whenAllPocketPropertiesProvided_thenItShouldReturnAllProperties") {
                    let currency = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let deadline = Date(timeIntervalSince1970: 997654327)
                    let goalParams = CreatePocketGoalRequestParams(amountCurrency: currency, deadline: deadline)
                    let params = CreatePocketPostRequestParams(name: "Home", icon: "home", goal: goalParams)
                    let entryParams = CreatePocketReview.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketReviewViewModel(entryParams: entryParams)

                    let output = viewModel.bind(confirmPocket: .never(), termsAndConditions: .never())

                    expect(output.isLoading)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false)]))

                    let pocketName = try! output.pocketName.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketName, "Home")

                    let pocketImage = try! output.pocketImage.asObservable().take(1).toBlocking().first() as? UIImage
                    XCTAssertNotNil(pocketImage)

                    let pocketAmount = try! output.pocketGoalAmount.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketAmount?.0, "$1,234")

                    let pocketDate = try! output.pocketGoalDateString.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketDate, "Goal date: Aug 13, 2001")

                    let edgeCase = try! output.edgeCaseAlert.asObservable().take(1).toBlocking().first() as? EdgeCase
                    XCTAssertNil(edgeCase)
                }

                it("givenViewModel_whenMandatoryPocketPropertiesProvided_thenItShouldReturnMandatoryPropertiesOnly") {
                    let params = CreatePocketPostRequestParams(name: "Home", icon: "home")
                    let entryParams = CreatePocketReview.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketReviewViewModel(entryParams: entryParams)

                    let output = viewModel.bind(confirmPocket: .never(), termsAndConditions: .never())

                    expect(output.isLoading)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false)]))

                    let pocketName = try! output.pocketName.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketName, "Home")

                    let pocketImage = try! output.pocketImage.asObservable().take(1).toBlocking().first() as? UIImage
                    XCTAssertNotNil(pocketImage)

                    let pocketAmount = try! output.pocketGoalAmount.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketAmount?.0, "No goal amount")

                    let pocketDate = try! output.pocketGoalDateString.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(pocketDate, "No goal date")

                    let edgeCase = try! output.edgeCaseAlert.asObservable().take(1).toBlocking().first() as? EdgeCase
                    XCTAssertNil(edgeCase)
                }

                it("givenViewModel_whenAllPocketPropertiesAreProvided_andContinueSelected_thenItShouldCreateThePocket") {
                    let clientCurrency = PocketsClient2.Currency(amount: "1234", currencyCode: "USD")
                    let deadline = Date(timeIntervalSince1970: 997654327)
                    let clientPocketGoal = PocketsClient2.PocketGoal(amountCurrency: clientCurrency, deadline: deadline, progress: 15)
                    let clientPocket = PocketsClient2.Pocket(id: "1234",
                                                             arrangementId: "someid",
                                                             name: "Home",
                                                             icon: "home",
                                                             goal: clientPocketGoal,
                                                             balance: clientCurrency)

                    client.postPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 200, header: [:], body: clientPocket)))]

                    let currency = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let goalParams = CreatePocketGoalRequestParams(amountCurrency: currency, deadline: deadline)
                    let params = CreatePocketPostRequestParams(name: "Home", icon: "home", goal: goalParams)
                    let entryParams = CreatePocketReview.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketReviewViewModel(entryParams: entryParams)

                    var createdPocket: RetailPocketsJourney.Pocket?
                    viewModel.didCreatePocket = { exitParams in
                        createdPocket = exitParams.pocket
                    }

                    let continueTap = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(confirmPocket: continueTap.asObservable(),
                                                termsAndConditions: .never())

                     expect(output.isLoading)
                         .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false),
                                   .next(10, true),
                                   .next(10, false)]))

                    let expectedGoal = RetailPocketsJourney.PocketGoal(amountCurrency: currency, deadline: deadline, progress: 15)
                    let expectedPocket = RetailPocketsJourney.Pocket(identifier: "1234", arrangementId: "someid", name: "Home", icon: "home", goal: expectedGoal, balance: currency)
                    expect(createdPocket).to(equal(expectedPocket))

                    let edgeCase = try! output.edgeCaseAlert.asObservable().take(1).toBlocking().first() as? EdgeCase
                    XCTAssertNil(edgeCase)
                }

                it("givenViewModel_whenAllPocketPropertiesAreProvided_andContinueSelected_andSomethingWentWrong_thenItShouldNOTCreateThePocket_andReturnEdgeCaseError") {
                    let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
                    client.postPocketCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, error)))]

                    let currency = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let deadline = Date(timeIntervalSince1970: 997654327)
                    let goalParams = CreatePocketGoalRequestParams(amountCurrency: currency, deadline: deadline)
                    let params = CreatePocketPostRequestParams(name: "Home", icon: "home", goal: goalParams)
                    let entryParams = CreatePocketReview.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketReviewViewModel(entryParams: entryParams)

                    var createdPocket: RetailPocketsJourney.Pocket?
                    viewModel.didCreatePocket = { exitParams in
                        createdPocket = exitParams.pocket
                    }

                    let continueTap = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(confirmPocket: continueTap.asObservable(),
                                                termsAndConditions: .never())

                     expect(output.isLoading)
                         .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false),
                                   .next(10, true),
                                   .next(10, false)]))
                    expect(createdPocket).to(beNil())

                    let edgeCase = try! output.edgeCaseAlert.asObservable().take(1).toBlocking().first() as? EdgeCase
                    XCTAssertEqual(edgeCase?.title, "Failed to create pocket")
                    XCTAssertEqual(edgeCase?.subtitle, "Unable to proceed, please try again.")
                    XCTAssertNil(edgeCase?.icon)
                    XCTAssertEqual(edgeCase?.actionButtonTitle, "OK")
                }
            }
        }
    }
}
