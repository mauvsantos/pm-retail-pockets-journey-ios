//
//  Created by Backbase R&D B.V. on 17/02/2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

class CreatePocketGoalViewModelTests: QuickSpec {
    override func spec() {
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!

        beforeEach {
            scheduler = TestScheduler(initialClock: 0)

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("CreatePocketGoal") {

                it("givenViewModel_whenAGoalAmountIsEntered_andContinueSelected_thenItShouldTriggerDidSelectContinue") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketAmount: String?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketAmount = item?.goal?.amountCurrency?.amount
                    }

                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(10, "1000")])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: amount.asObservable(),
                                           date: .never(),
                                           continueTap: continueTap.asObservable(),
                                           skipTap: .never())

                    scheduler.advanceTo(30)
                    expect(pocketAmount).to(equal("1000"))
                }

                it("givenViewModel_whenAGoalDateIsSelected_andContinueSelected_thenItShouldTriggerDidSelectContinue") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketDate: Date?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketDate = item?.goal?.deadline
                    }

                    let date: TestableObservable<Date?> = scheduler.createColdObservable([.next(10, Date(timeIntervalSince1970: 997654327))])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: .never(),
                                           date: date.asObservable(),
                                           continueTap: continueTap.asObservable(),
                                           skipTap: .never())

                    scheduler.advanceTo(30)
                    expect(pocketDate?.description).to(equal("2001-08-12 22:12:07 +0000"))
                }

                it("givenViewModel_whenAGoalDateIsSelected_andAmountIsChanged_andContinueSelected_thenItShouldTriggerDidSelectContinue") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketDate: Date?
                    var pocketAmount: String?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketDate = item?.goal?.deadline
                        pocketAmount = item?.goal?.amountCurrency?.amount
                    }

                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(10, "2000")])
                    let date: TestableObservable<Date?> = scheduler.createColdObservable([.next(10, Date(timeIntervalSince1970: 997654327))])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: amount.asObservable(),
                                           date: date.asObservable(),
                                           continueTap: continueTap.asObservable(),
                                           skipTap: .never())

                    scheduler.advanceTo(30)
                    expect(pocketDate?.description).to(equal("2001-08-12 22:12:07 +0000"))
                    expect(pocketAmount).to(equal("2000"))
                }

                it("givenViewModel_whenAGoalAmountIsEntered_andSkipSelected_thenItShouldTriggerDidSelectContinueWithoutGoal") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketAmount: String?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketAmount = item?.goal?.amountCurrency?.amount
                    }

                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(10, "1000")])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: amount.asObservable(),
                                           date: .never(),
                                           continueTap: .never(),
                                           skipTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    expect(pocketAmount).to(beNil())
                }

                it("givenViewModel_whenAGoalDateIsSelected_andContinueSelected_thenItShouldTriggerDidSelectContinueWithoutGoal") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketDate: Date?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketDate = item?.goal?.deadline
                    }

                    let date: TestableObservable<Date?> = scheduler.createColdObservable([.next(10, Date(timeIntervalSince1970: 997654327))])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: .never(),
                                           date: date.asObservable(),
                                           continueTap: .never(),
                                           skipTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    expect(pocketDate).to(beNil())
                }

                it("givenViewModel_whenAGoalDateIsSelected_andAmountIsChanged_andContinueSelected_thenItShouldTriggerDidSelectContinueWithoutGoal") {
                    let params = CreatePocketPostRequestParams(name: "Some name", icon: "some icon")
                    let entryParams = CreatePocketGoal.EntryParams(createPocketPostRequestParams: params)
                    let viewModel = CreatePocketGoalViewModel(entryParams: entryParams)

                    var pocketDate: Date?
                    var pocketAmount: String?
                    viewModel.didSelectContinue = { params in
                        let item = params.createPocketPostRequestParams
                        pocketDate = item?.goal?.deadline
                        pocketAmount = item?.goal?.amountCurrency?.amount
                    }

                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(10, "2000")])
                    let date: TestableObservable<Date?> = scheduler.createColdObservable([.next(10, Date(timeIntervalSince1970: 997654327))])
                    let continueTap = scheduler.createColdObservable([.next(20, ())])

                    let _ = viewModel.bind(amount: amount.asObservable(),
                                           date: date.asObservable(),
                                           continueTap: .never(),
                                           skipTap: continueTap.asObservable())

                    scheduler.advanceTo(30)
                    expect(pocketDate?.description).to(beNil())
                    expect(pocketAmount).to(beNil())
                }
            }
        }
    }
}
