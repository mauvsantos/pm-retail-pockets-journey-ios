//
//  Created by Backbase on 19.04.2021.
//

import Foundation

private final class BundleToken {}

extension Bundle {
    static func data(from fileName: String) -> Data? {
        let testsBundle = Bundle(for: BundleToken.self)
        guard
            let url = testsBundle.url(forResource: fileName, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            return nil
        }
        return data
    }
}
