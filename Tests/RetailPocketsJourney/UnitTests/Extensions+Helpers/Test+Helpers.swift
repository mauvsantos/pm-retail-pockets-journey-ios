//
//  Created by Backbase on 19.04.2021.
//

import Foundation
import Nimble
import RxNimble
import RxTest
import RxSwift

func equal<O, T>(as: T.Type, _ expectedEvents: RecordedEvents<O?>) -> Predicate<[Recorded<Event<O?>>]> where T: Equatable {
    return Predicate<[Recorded<Event<O?>>]> { expression -> PredicateResult in
        do {
            guard let evaluated = try expression.evaluate() else {
                return .init(bool: false, message: .expectedTo("receive \(expectedEvents.debugDescription)"))
            }
            guard evaluated.count == expectedEvents.count else {
                return .init(bool: false, message: .expectedCustomValueTo("receive \(expectedEvents.debugDescription)", actual: evaluated.debugDescription))
            }

            var eventsMatching = true
            for (idx, recordedEvent) in evaluated.enumerated() {
                let expected = expectedEvents[idx]
                guard expected.time == recordedEvent.time else {
                    eventsMatching = false
                    break
                }
                if case let expectedElement?? = expected.value.element {
                    if case let recordedElement?? = recordedEvent.value.element, let recordedError = recordedElement as? T,
                        let expectedError = expectedElement as? T {
                        eventsMatching = recordedError == expectedError
                    } else {
                        eventsMatching = false
                        break
                    }
                } else if case _?? = recordedEvent.value.element {
                    eventsMatching = false
                    break
                }
            }
            return .init(bool: eventsMatching, message: .expectedCustomValueTo("receive \(expectedEvents.debugDescription)", actual: evaluated.debugDescription))
        } catch {
            return .init(bool: false, message: .expectedTo("receive \(expectedEvents.debugDescription)"))
        }
    }
}

