//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import ArrangementsClient2
import ClientCommon

private final class BundleToken {}

private extension Bundle {
    static func data(from fileName: String) -> Data? {
        let testsBundle = Bundle(for: BundleToken.self)
        guard
            let url = testsBundle.url(forResource: fileName, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            return nil
        }
        return data
    }
}

class MockArrangementsClient: ProductSummaryAPIProtocol {
    enum MockError: Error {
        case generalError
    }

    var arrangementsCallMocks: [MockCall<[ProductSummaryItem]>] = []
    var errors: [Error] = []

    var successfulArrangementsMocks: [MockCall<[ProductSummaryItem]>] {
        guard let data = Bundle.data(from: "arrangements") else { return [] }
        let response = try! JSONDecoder().decode([ProductSummaryItem].self, from: data)
        return [.init(expectedResult: .success(.init(statusCode: 201,
                                                     header: [:],
                                                     body: response)))]
    }

    func getArrangementsByBusinessFunctionCall(businessFunction: String,
                                               resourceName: String,
                                               privilege: String,
                                               contentLanguage: String?,
                                               withLatestBalances: Bool?,
                                               maskIndicator: Bool?,
                                               debitAccount: Bool?,
                                               creditAccount: Bool?,
                                               externalTransferAllowed: Bool?,
                                               productKindName: String?,
                                               legalEntityIds: [String]?,
                                               sourceId: String?,
                                               favorite: Bool?,
                                               searchTerm: String?,
                                               customOrder: Bool?,
                                               favoriteFirst: Bool?,
                                               from: Int?,
                                               size: Int?,
                                               cursor: String?,
                                               orderBy: [OrderByField]?,
                                               direction: SortDirection?) throws -> Call<[ProductSummaryItem]> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return arrangementsCallMocks.removeFirst()
    }

    func getProductSummaryCall(contentLanguage: String?,
                               debitAccount: Bool?,
                               creditAccount: Bool?,
                               maskIndicator: Bool?) throws -> Call<ProductSummary> {
        throw MockError.generalError
    }

    func getProductSummaryEntitlementsByLegalEntityIdCall(legalEntityIds: [String],
                                                          arrangementIds: [String]?,
                                                          ignoredArrangementIds: [String]?,
                                                          searchTerm: String?,
                                                          from: Int?,
                                                          size: Int?,
                                                          cursor: String?,
                                                          orderBy: [OrderByField]?,
                                                          direction: SortDirection?) throws -> Call<[ProductSummaryItem]> {
        throw MockError.generalError
    }

    func postFilterProductSummariesCall(productSummaryFilterParams: ProductSummaryFilterParams) throws -> Call<ProductSummaryFilterResult> {
        throw MockError.generalError
    }
}
