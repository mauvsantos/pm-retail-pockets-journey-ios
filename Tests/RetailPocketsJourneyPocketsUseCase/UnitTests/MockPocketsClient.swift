//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import PocketsClient2
import ClientCommon

class McokPocketsClient: PocketTailorClientAPIProtocol {
    enum MockError: Error {
        case generalError
    }

    var closePocketCallMocks: [MockCall<NoResponse>] = []
    var listPocketsCallMocks: [MockCall<PocketsClient2.PocketListResponse>] = []
    var postPocketCallMocks: [MockCall<PocketsClient2.Pocket>] = []
    var viewPocketCallMocks: [MockCall<PocketsClient2.Pocket>] = []
    var errors: [Error] = []

    func setupDefaultSuccess() {
        let pocket = Pocket(id: "1",
                            arrangementId: "arrangementId",
                            name: "Pocket Name",
                            icon: "icon",
                            balance: Currency(amount: "123", currencyCode: "USD"))
        closePocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 204,
                                                                     header: [:],
                                                                     body: Data())))]
        listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                     header: [:],
                                                                     body: PocketListResponse(pockets: [pocket]))))]
        postPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                    header: [:],
                                                                    body: pocket)))]
        viewPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                    header: [:],
                                                                    body: pocket)))]
    }

    func setupDefaultFailures() {
        closePocketCallMocks = [.init(expectedResult: .failure(.error(500, nil, CallError.unsuccessfulHTTPStatusCode(nil))))]
        listPocketsCallMocks = [.init(expectedResult: .failure(.error(500, nil, CallError.unsuccessfulHTTPStatusCode(nil))))]
        postPocketCallMocks = [.init(expectedResult: .failure(.error(500, nil, CallError.unsuccessfulHTTPStatusCode(nil))))]
        viewPocketCallMocks = [.init(expectedResult: .failure(.error(500, nil, CallError.unsuccessfulHTTPStatusCode(nil))))]
    }

    func setupEmptyResponse() {
        closePocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 204,
                                                                     header: [:],
                                                                     body: nil)))]
        listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                     header: [:],
                                                                     body: nil)))]
        postPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                    header: [:],
                                                                    body: nil)))]
        viewPocketCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                    header: [:],
                                                                    body: nil)))]
    }

    func closePocketCall(closePocketRequest: PocketsClient2.ClosePocketRequest) throws -> Call<NoResponse> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return closePocketCallMocks.removeFirst()
    }
    func listPocketsCall() throws -> Call<PocketsClient2.PocketListResponse> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return listPocketsCallMocks.removeFirst()
    }
    func postPocketCall(pocketPostRequest pocketPost: PocketsClient2.PocketPostRequest) throws -> Call<PocketsClient2.Pocket> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return postPocketCallMocks.removeFirst()
    }
    func viewPocketCall(pocketId: String) throws -> Call<PocketsClient2.Pocket> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return viewPocketCallMocks.removeFirst()
    }
}
