//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import XCTest
import PaymentOrderClient2
import ClientCommon
import Backbase
import Resolver
import RetailPocketsJourney
@testable import RetailPocketsJourneyPaymentsUseCase

class PaymentsUseCaseTests: XCTestCase {
    var client: MockPaymentsClient!
    var paymentsUseCase: PaymentsServiceUseCase!

    var paymentOrderPost: RetailPocketsJourney.PaymentOrdersPost {
        let identification = RetailPocketsJourney.Identification(identification: "1234", schemeName: .id)
        let accountIdentification = RetailPocketsJourney.AccountIdentification(identification: identification)
        let involvedParty = RetailPocketsJourney.InvolvedParty(name: "some name")
        let initiateCounterpartyAccount = RetailPocketsJourney.InitiateCounterpartyAccount(accountType: "some type",
                                                                                           selectedContact: nil,
                                                                                           identification: identification,
                                                                                           name: nil)
        let currency = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
        let intiateTransaction = RetailPocketsJourney.InitiateTransaction(counterparty: involvedParty,
                                                                          counterpartyAccount: initiateCounterpartyAccount,
                                                                          instructedAmount: currency)
        return RetailPocketsJourney.PaymentOrdersPost(originatorAccount: accountIdentification,
                                                      requestedExecutionDate: Date(),
                                                      transferTransactionInformation: intiateTransaction)
    }

    override func setUpWithError() throws {
        try super.setUpWithError()

        try Backbase.initialize("config-stable.json", forceDecryption: false)
        client = MockPaymentsClient()
        paymentsUseCase = PaymentsUseCase(client: client)
        Resolver.register { self.client as PaymentOrdersAPIProtocol }
    }

    override func tearDown() {
        super.tearDown()

        Resolver.reset()
    }

    func test_givenPocketTransfer_whenPostPaymentOrderCalled_thenTheResultShouldBeSuccessfull() {
        let exchangeRateInformation = PaymentOrderClient2.ExchangeRateInformation(currencyCode: "USD",
                                                                                  rate: "0.2",
                                                                                  rateType: .agreed,
                                                                                  contractIdentification: "some contract identification")
        let transferFee = PaymentOrderClient2.Currency(amount: "1234", currencyCode: "USD")
        let dec2030 = Date(timeIntervalSinceReferenceDate: 60 * 60 * 24 * 365 * 30) // Dec 25, 2030
        let response = PaymentOrderClient2.PaymentOrdersPostResponse(id: "2345",
                                                                     status: .entered,
                                                                     bankStatus: "some status",
                                                                     reasonCode: "some reason code",
                                                                     reasonText: "some reason text",
                                                                     errorDescription: "some error description",
                                                                     nextExecutionDate: dec2030,
                                                                     paymentSetupId: "some payment setup id",
                                                                     paymentSubmissionId: "some submission id",
                                                                     approvalStatus: "some approval status",
                                                                     transferFee: transferFee,
                                                                     exchangeRateInformation: exchangeRateInformation,
                                                                     additions: ["something": "something value"])

        client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                 header: [:],
                                                                                 body: response)))]
        let expectation = XCTestExpectation()


        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(let successResponse):
                XCTAssertEqual(successResponse.identifier, "2345")
                XCTAssertEqual(successResponse.status, .entered)
                XCTAssertEqual(successResponse.bankStatus, "some status")

                XCTAssertEqual(successResponse.reasonCode, "some reason code")
                XCTAssertEqual(successResponse.reasonText, "some reason text")
                XCTAssertEqual(successResponse.errorDescription, "some error description")
                XCTAssertEqual(successResponse.nextExecutionDate?.description, "2030-12-25 00:00:00 +0000")
                XCTAssertEqual(successResponse.paymentSetupId, "some payment setup id")
                XCTAssertEqual(successResponse.paymentSubmissionId, "some submission id")
                XCTAssertEqual(successResponse.approvalStatus, "some approval status")
                XCTAssertEqual(successResponse.transferFee?.amount, "1234")
                XCTAssertEqual(successResponse.transferFee?.currencyCode, "USD")
                XCTAssertEqual(successResponse.exchangeRateInformation?.currencyCode, "USD")
                XCTAssertEqual(successResponse.exchangeRateInformation?.rate, "0.2")
                XCTAssertEqual(successResponse.exchangeRateInformation?.rateType, .agreed)
                XCTAssertEqual(successResponse.exchangeRateInformation?.contractIdentification, "some contract identification")
                XCTAssertEqual(successResponse.additions?["something"], "something value")

            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenNoInternetAccessWithNSError_whenPostPaymentOrderCalled_ThenItShouldFailDueToNoInternet() {
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, error)))]
        let expectation = XCTestExpectation()

        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertNotNil(errorResponse)
                XCTAssertNotNil(errorResponse.error)
                XCTAssertEqual(errorResponse.statusCode, NSURLErrorNotConnectedToInternet)
                XCTAssertEqual(errorResponse.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.notConnected)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenNoInternetAccessWithCallError_whenPostPaymentOrderCalled_ThenItShouldFailDueToNoInternet() {
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, CallError.unsuccessfulHTTPStatusCode(error))))]
        let expectation = XCTestExpectation()

        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertNotNil(errorResponse)
                XCTAssertNotNil(errorResponse.error)
                XCTAssertEqual(errorResponse.statusCode, NSURLErrorNotConnectedToInternet)
                XCTAssertEqual(errorResponse.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.notConnected)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenGeneralErrorWithNSError_whenPostPaymentOrderCalled_ThenItShouldFailDueToLoadingFailed() {
        let error = NSError(domain: "", code: 403, userInfo: nil)
        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(.error(403, nil, error)))]
        let expectation = XCTestExpectation()

        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertNotNil(errorResponse)
                XCTAssertNotNil(errorResponse.error)
                XCTAssertEqual(errorResponse.statusCode, 403)
                XCTAssertEqual(errorResponse.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.loadingFailure(underlying: error))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenGeneralErrorWithCallError_whenPostPaymentOrderCalled_ThenItShouldFailDueToLoadingFailed() {
        let error = NSError(domain: "", code: 403, userInfo: nil)
        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(.error(403, nil, CallError.unsuccessfulHTTPStatusCode(error))))]
        let expectation = XCTestExpectation()

        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertNotNil(errorResponse)
                XCTAssertNotNil(errorResponse.error)
                XCTAssertEqual(errorResponse.statusCode, 403)
                XCTAssertEqual(errorResponse.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.loadingFailure(underlying: error))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenInvalidSuccessResponse_whenGetArrangementsCalled_ThenItShouldFailDueToInvalidResponse() {
        client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                 header: [:],
                                                                                 body: nil)))]
        let expectation = XCTestExpectation()
        paymentsUseCase.postPaymentOrder(paymentOrderPost) { result in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertNotNil(errorResponse)
                XCTAssertNotNil(errorResponse.error)
                XCTAssertEqual(errorResponse.statusCode, 0)
                XCTAssertEqual(errorResponse.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.invalidResponse)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
}
